from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'Capas S.A.C.'
settings.subtitle = 'powered by Julio Laveriano'
settings.author = 'Julio Laveriano'
settings.author_email = 'julioantonio.lp@gmail.com'
settings.keywords = 'capas, capassac, aji, barranca, lima'
settings.description = 'Proyecto Capas S.A.C. hecho por Julio Laveriano'
settings.layout_theme = 'orangeflower'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'bee87619-7ee0-4a19-89e8-5fd0120db483'
settings.email_server = 'localhost'
settings.email_sender = 'julioantonio.lp@gmail.com'
settings.email_login = 'julioantonio.lp@gmail.com'
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
