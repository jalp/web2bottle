response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
response.menu = [
(T('Index'),URL('default','index')==URL(),URL('default','index'),[]),
(T('Producto'),URL('default','producto')==URL(),URL('default','producto'),[]),
(T('Cliente'),URL('default','cliente')==URL(),URL('default','cliente'),[]),
(T('La Empresa'),URL('default','la_empresa')==URL(),URL('default','la_empresa'),[]),
]

#MENU PARA MOSTRAR PAGINAS EN WEB2PY Y BOTTLE

def _():
    # shortcuts
    app = request.application
    ctr = request.controller
    # useful links to internal and external resources
    response.menu+=[
        (SPAN('Web2Bottle',_style='color:darkorange'),False, '#', [
                (SPAN('::Web2py'),URL('default','web2bottle')==URL(),URL('default','web2bottle'),[]),                
                (T('::Bottle'),URL('default','mi_pedido')==URL(),URL('default','mi_pedido'),[]),
                (T('::Author'),URL('default','author')==URL(),URL('default','author'),[]),
                #(T('::Bottle'),False,('http://localhost:8080/twoports'))
                ]
         )]
_()
